import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
export const TasksCollection = new Mongo.Collection('tasks');

Meteor.methods({
  'tasks.insert'(title) {
    check(title, String);

    if (!this.userId) {
      throw new Meteor.Error('not authorized');
    }

    TasksCollection.insert({
      title,
      createdAt: new Date(),
      owner: this.userId,
      username: Meteor.users.findOne(this.userId).username,
      isChecked: false
    });
  },
  'tasks.remove'(taskId) {
    check(taskId, String);
    const task = TasksCollection.findOne(taskId);
 
    if (!this.userId || task.owner !== this.userId) {
      throw new Meteor.Error('not authorized');
    }

    TasksCollection.remove(taskId);
  },
  'tasks.setChecked'(taskId, isChecked) {
    check(taskId, String);
    check(isChecked, Boolean);
    const task = TasksCollection.findOne(taskId);
 
    if (!this.userId || task.owner !== this.userId) {
      throw new Meteor.Error('not authorized');
    }

    TasksCollection.update(taskId, { $set: { isChecked } });
  },
  'tasks.setPrivate'(taskId, isPrivate) {
    check(taskId, String);
    check(isPrivate, Boolean);
    const task = TasksCollection.findOne(taskId);
 
    if (!this.userId || task.owner !== this.userId) {
      throw new Meteor.Error('not authorized');
    }

    TasksCollection.update(taskId, { $set: { isPrivate } });
  },
});

if (Meteor.isServer) {
  Meteor.publish('tasks', function () {
    return TasksCollection.find({
      $or: [
        { isPrivate: { $ne: true } },
        { owner: this.userId }
      ]
    });
  });
}
