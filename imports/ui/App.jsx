import React, { useState } from 'react';
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import _ from 'lodash';
import { Task } from './Task.jsx';
import { TasksCollection } from '../api/tasks.js';
import { TaskForm } from './TaskForm.jsx';
import { LoginForm } from './LoginForm.jsx';

const changeTaskStatus = ({ _id, isChecked }) => {
  Meteor.call('tasks.setChecked', _id, !isChecked);
};

const deleteTaskById = (_id) => {
  Meteor.call('tasks.remove', _id);
};

const changePrivateStatus = ({ _id, isPrivate }) => {
  Meteor.call('tasks.setPrivate', _id, !isPrivate);
};

export const App = () => {
  const filter = {};
  const [hideCompleted, setHideCompleted] = useState(false);
  const onChangeHideCompleted = () => {
    setHideCompleted(!hideCompleted);
  };
  if (hideCompleted) {
    _.set(filter, 'isChecked', false);
  }
  const { tasks, incompleteTasksCount, user } = useTracker(() => {
    Meteor.subscribe('tasks');
    return {
      tasks: TasksCollection.find(filter, { sort: { createdAt: -1 } }).fetch(),
      incompleteTasksCount: TasksCollection.find({
        isChecked: { $ne: true },
      }).count(),
      user: Meteor.user(),
    };
  });

  if (!user) {
    return (
      <div className="simple-todos-react">
        <LoginForm />
      </div>
    );
  }

  return (
    <div className="simple-todos-react">
      <div className="header">
        <h1>todos ({incompleteTasksCount}) </h1>
        <div className="filters">
          <input
            type="checkbox"
            readOnly
            checked={hideCompleted}
            onClick={onChangeHideCompleted}
          />
          <label>hide completed tasks</label>
        </div>
      </div>
      <div>
        <TaskForm user={user} />
      </div>
      <ul className="tasks">
        {tasks.map((task) => (
          <Task
            key={task._id}
            task={task}
            changeTaskStatus={changeTaskStatus}
            changePrivateStatus={changePrivateStatus}
            deleteTask={deleteTaskById}
          />
        ))}
      </ul>
    </div>
  );
};
