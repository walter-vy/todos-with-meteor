import React, { useState } from 'react';

export const LoginForm = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const onChangeUsername = (e) => setUsername(e.target.value);
  const onChangePassword = (e) => setPassword(e.target.value);

  const submit = (e) => {
    e.preventDefault();
    Meteor.loginWithPassword(username, password);
  };

  return (
    <form className="login-form" onSubmit={submit}>
      <h1>login</h1>
      <label htmlFor="username">username</label>
      <input
        type="text"
        id="username"
        placeholder="Username"
        required
        value={username}
        onChange={onChangeUsername}
      />
      <label htmlFor="passowrd">password</label>
      <input
        type="password"
        id="password"
        placeholder="password"
        required
        value={password}
        onChange={onChangePassword}
      />
      <button type="submit">Log In</button>
    </form>
  );
};
