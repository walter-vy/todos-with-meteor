import React from 'react';

export const Task = ({
  task,
  changeTaskStatus,
  changePrivateStatus,
  deleteTask,
}) => {
  const isChecked = Boolean(task.isChecked);
  const isPrivate = Boolean(task.isPrivate);
  const handleClickCheckBox = () => {
    changeTaskStatus(task);
  };
  const handleDeleteTask = () => {
    deleteTask(task._id);
  };

  const handleChangePrivateStatus = () => {
    changePrivateStatus(task)
  };

  return (
    <li className={`task ${isChecked ? 'checked' : ''} ${isPrivate ? 'private' : ''} ` }>
      <div className="left">
        <input
          type="checkbox"
          checked={isChecked}
          onClick={handleClickCheckBox}
          readOnly
        />
        <button onClick={handleChangePrivateStatus}> {isPrivate ? 'private' : 'public'} </button>
        <span>{task.title}</span>
      </div>
      <div className="right">
        <button onClick={handleDeleteTask}>&times;</button>
      </div>
    </li>
  );
};
