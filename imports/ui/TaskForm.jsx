import React, { useState } from 'react';
import { Meteor } from 'meteor/meteor'
import { TasksCollection } from '../api/tasks';

export const TaskForm = ({user}) => {
  const [text, setText] = useState('');

  const handleChangeTitle = (e) => {
    setText(e.target.value);
  };
  const handleSubmitForm = (e) => {
    e.preventDefault()
    let title = text.trim();
    if (!title) return;
    Meteor.call('tasks.insert', title)
    setText('');
  };

  return (
    <form className="task-form" onSubmit={handleSubmitForm}>
      <input
        type="text"
        onChange={handleChangeTitle}
        placeholder="type to add new task"
        value={text}
      />
      <button type="submit">add task</button>
    </form>
  );
};
